module.exports = {
  env: {
    node: true,
  },
  extends: [
    'airbnb',
  ],
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module',
  },
  rules: {
    'no-underscore-dangle': 'off',
    'no-use-before-define': 'off',
    'no-param-reassign': 'off',
    'linebreak-style': 'off',
  },
  ignorePatterns: ['*.test.js'],
};
