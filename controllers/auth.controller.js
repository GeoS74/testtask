const jwt = require('jsonwebtoken');

const User = require('../models/User');
const passport = require('../libs/passport');
const config = require('../config.json');
const logger = require('../services/logger')(module);

module.exports = {
  signup,
  authorized,
};

async function signup(req, res) {
  try {
    await _createUser(req.body);

    return res.status(201).json({
      message: 'user create',
    });
  } catch (error) {
    if (error.name === 'ValidationError') {
      return res.status(400).json({
        error: 'login not unique',
      });
    }

    logger.error(error.message);
    return res.status(500).end();
  }
}

async function authorized(req, res) {
  await passport.authenticate('local', async (error, user, info) => {
    if (error) {
      logger.error(error.message);
      return res.status(500).end();
    }

    if (!user) {
      return res.status(400).json(info);
    }

    const token = await _createToken(user);

    res.header('Authorization', `Bearer ${token}`);
    return res.status(200).json({
      message: 'authorization ok',
    });
  })(req);
}

async function _createUser(data) {
  const user = User({
    name: data.login,
  });
  await user.setPassword(data.password);
  await user.save();
  return user;
}

async function _createToken(user) {
  const { name } = user;
  const token = jwt.sign(
    { name },
    config.app,
    {
      expiresIn: config.jwt_ttl,
    },
  );

  return token;
}
