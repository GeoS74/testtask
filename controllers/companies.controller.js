const Company = require('../models/Company');
const logger = require('../services/logger')(module);

module.exports = {
  getAll,
  get,
  update,
  add,
  del,
};

async function add(req, res) {
  try {
    const company = await _createCompany(req.body);

    return res.status(201).json({
      message: `company created id:${company.id}`,
    });
  } catch (error) {
    if (error.name === 'ValidationError') {
      return res.status(400).json({
        error: error.message,
      });
    }

    logger.error(error.message);
    return res.status(500).end();
  }
}

async function update(req, res) {
  try {
    const company = await _updateCompany(req.body, req.params.id);
    const result = _formatData(company);

    return res.status(200).json(result);
  } catch (error) {
    return res.status(404).json({
      error: 'company not found',
    });
  }
}

async function del(req, res) {
  try {
    const result = await _deleteCompany(req.params.id);

    if (!result.deletedCount) {
      throw new Error();
    }

    return res.status(200).json({
      message: `company deleted id: ${req.params.id}`,
    });
  } catch (error) {
    return res.status(404).json({
      error: 'company not found',
    });
  }
}

async function get(req, res) {
  try {
    const company = await _getCompany(req.params.id);
    const result = _formatData(company);

    return res.status(200).json(result);
  } catch (error) {
    return res.status(404).json({
      error: 'company not found',
    });
  }
}

async function getAll(req, res) {
  const companies = await _getCompany(null, req.query);
  const result = companies.map((company) => _formatData(company));

  res.status(200).json(result);
}

async function _createCompany(company) {
  return Company.create({
    contactId: company.contactId || undefined,
    name: company.name,
    shortName: company.shortName || undefined,
    businessEntity: company.businessEntity || undefined,
    contract: company.contract,
    type: company.type,
    status: company.status || undefined,
    address: company.address || undefined,
  });
}

async function _updateCompany(company, id) {
  return Company.findOneAndUpdate(
    { _id: id },
    company,
    { new: true },
  )
    .populate('photos');
}

function _deleteCompany(id) {
  return Company.deleteOne({
    _id: id,
  });
}

function _getCompany(id, query) {
  if (id) {
    return Company
      .findOne({ _id: id })
      .populate('photos');
  }

  const conditions = {};
  if (query.status) conditions.status = query.status;
  if (query.type) conditions.type = query.type;

  const limit = query.limit || '';
  const skip = query.start || '';

  const order = +query.order === 1 ? 1 : -1;
  const sorted = {};
  if (query.sort === 'name') sorted.name = order;
  else if (query.sort === 'date') sorted.createdAt = order;
  else sorted._id = order;

  return Company
    .find(conditions)
    .sort(sorted)
    .limit(limit)
    .skip(skip)
    .populate('photos');
}

function _formatData(company) {
  return {
    id: company.id,
    contactId: company.contactId || null,
    name: company.name || null,
    shortName: company.shortName || null,
    businessEntity: company.businessEntity || null,
    contract: company.contract,
    type: company.type,
    status: company.status || null,
    address: company.address || null,
    createdAt: company.createdAt,
    updatedAt: company.updatedAt,
    photos: company.photos.map((photo) => ({
      name: photo.name,
      filepath: photo.filepath,
      thumbpath: photo.thumbpath,
    })),
  };
}
