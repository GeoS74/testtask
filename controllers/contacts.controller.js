const Contact = require('../models/Contact');
const logger = require('../services/logger')(module);

module.exports = {
  getAll,
  get,
  update,
  add,
  del,
};

async function add(req, res) {
  try {
    const contact = await _createContact(req.body);

    return res.status(201).json({
      message: `contact created id:${contact.id}`,
    });
  } catch (error) {
    if (error.name === 'ValidationError') {
      return res.status(400).json({
        error: error.message,
      });
    }

    logger.error(error.message);
    return res.status(500).end();
  }
}

async function update(req, res) {
  try {
    const contact = await _updateContact(req.body, req.params.id);
    const result = _formatData(contact);

    return res.status(200).json(result);
  } catch (error) {
    return res.status(404).json({
      error: 'contact not found',
    });
  }
}

async function del(req, res) {
  try {
    const result = await _deleteContact(req.params.id);

    if (!result.deletedCount) {
      throw new Error();
    }

    return res.status(200).json({
      message: `contact deleted id: ${req.params.id}`,
    });
  } catch (error) {
    return res.status(404).json({
      error: 'contact not found',
    });
  }
}

async function get(req, res) {
  try {
    const contact = await _getContact(req.params.id);
    const result = _formatData(contact);

    return res.status(200).json(result);
  } catch (error) {
    return res.status(404).json({
      error: 'contact not found',
    });
  }
}

async function getAll(req, res) {
  const contacts = await _getContact();
  const result = contacts.map((cont) => _formatData(cont));

  res.status(200).json(result);
}

function _createContact(contact) {
  return Contact.create({
    lastname: contact.lastname || undefined,
    firstname: contact.firstname || undefined,
    patronymic: contact.patronymic || undefined,
    phone: contact.phone || undefined,
    email: contact.email || undefined,
  });
}

function _updateContact(contact, id) {
  return Contact.findOneAndUpdate(
    { _id: id },
    contact,
    { new: true },
  );
}

function _deleteContact(id) {
  return Contact.deleteOne({
    _id: id,
  });
}

function _getContact(id) {
  if (id) {
    return Contact.findOne({
      _id: id,
    });
  }

  return Contact.find();
}

function _formatData(contact) {
  return {
    id: contact._id,
    lastname: contact.lastname || null,
    firstname: contact.firstname || null,
    patronymic: contact.patronymic || null,
    phone: contact.phone || null,
    email: contact.email || null,
    updatedAt: contact.updatedAt,
    createdAt: contact.createdAt,
  };
}
