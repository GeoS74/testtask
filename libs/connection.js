const mongoose = require('mongoose');
const config = require('../config.json');

mongoose.plugin(require('mongoose-unique-validator'));

module.exports = mongoose.createConnection(config.mongodb.uri);
