const LocalStrategy = require('passport-local').Strategy;
const User = require('../../models/User');

module.exports = new LocalStrategy(
  {
    usernameField: 'login',
    passwordField: 'password',
    session: false,
  },
  (async (name, password, done) => {
    try {
      const user = await User.findOne({ name });

      if (!user) {
        return done(null, false, { error: 'user not found' });
      }

      if (!await user.checkPassword(password)) {
        return done(null, false, { error: 'incorrect password' });
      }

      return done(null, user);
    } catch (error) {
      return done(error);
    }
  }),
);
