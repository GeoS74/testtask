const httpContext = require('express-http-context');
const jwt = require('jsonwebtoken');

const logger = require('../services/logger')(module);
const config = require('../config.json');

module.exports = async (req, res, next) => {
  try {
    const token = req.headers.authorization.split(' ')[1];

    if (!token) {
      throw new Error();
    }

    const decoded = jwt.verify(token, config.app);

    if (new Date() > new Date(decoded.exp * 1000)) {
      throw new Error();
    }

    req.user = decoded.name;
    httpContext.set('user', decoded?.name);

    return next();
  } catch (error) {
    logger.error(error.message);
    return res.status(401).json({ error: 'Unauthorized' });
  }
};
