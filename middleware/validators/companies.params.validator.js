const mongoose = require('mongoose');

const Contact = require('../../models/Contact');
const Company = require('../../models/Company');
const logger = require('../../services/logger')(module);

module.exports = {
  addCompany,
  updateCompany,
  deleteCompany,
};

function addCompany(req, res, next) {
  try {
    if (!req.body?.name) {
      res.status(400);
      throw new Error('Company name is empty');
    }

    if (!!req.body?.contactId && !_checkContactId(req.body?.contactId)) {
      res.status(404);
      throw new Error('Contact not found');
    }

    req.body.contract = {
      no: req.body?.contractNumber || undefined,
      issue_date: req.body?.contractDate || undefined,
    };

    if (req.body?.type) {
      req.body.type = Array.isArray(req.body.type) ? req.body.type : [req.body.type];
    } else {
      req.body.type = [];
    }
  } catch (err) {
    const error = err.message;
    logger.error(error);
    return res.json({ error });
  }

  return next();
}

function updateCompany(req, res, next) {
  try {
    if (!_checkValidObjectId(req.params?.id)) {
      res.status(404);
      throw new Error('Company not found');
    }

    if (!_checkCompanyId(req.params?.id)) {
      res.status(404);
      throw new Error('Company not found');
    }

    if (!!req.body?.contactId && !_checkContactId(req.body?.contactId)) {
      res.status(404);
      throw new Error('Contact not found');
    }

    req.body.contract = {
      no: req.body?.contractNumber || undefined,
      issue_date: req.body?.contractDate || undefined,
    };

    if (req.body?.type) {
      req.body.type = Array.isArray(req.body.type) ? req.body.type : [req.body.type];
    } else {
      req.body.type = [];
    }
  } catch (err) {
    const error = err.message;
    logger.error(error);
    return res.json({ error });
  }

  return next();
}

function deleteCompany(req, res, next) {
  try {
    if (!_checkValidObjectId(req.params?.id)) {
      res.status(404);
      throw new Error('Company not found');
    }

    if (!_checkCompanyId(req.params?.id)) {
      res.status(404);
      throw new Error('Company not found');
    }
  } catch (err) {
    const error = err.message;
    logger.error(error);
    return res.json({ error });
  }

  return next();
}

async function _checkCompanyId(companyId) {
  const company = await Company.findOne({ _id: companyId });
  return !!company;
}

async function _checkContactId(contactId) {
  const contact = await Contact.findOne({ _id: contactId });
  return !!contact;
}

function _checkValidObjectId(id) {
  if (!id || !mongoose.Types.ObjectId.isValid(id)) {
    return false;
  }
  return true;
}
