// т.к. ни одно поле для контактов не является обязательным (в данной задаче это не оговорено),
// то валидатор проверяет только валидность идентификатора контакта

const mongoose = require('mongoose');

module.exports = (req, res, next) => {
  try {
    if (req.params?.id && !mongoose.Types.ObjectId.isValid(req.params?.id)) {
      res.status(404);
      throw new Error('Contact not found');
    }
  } catch (err) {
    const error = err.message;
    return res.json({ error });
  }

  return next();
};
