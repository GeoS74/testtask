const fs = require('fs');
const path = require('path');
const Company = require('../../models/Company');
const Photo = require('../../models/Photo');

const logger = require('../../services/logger')(module);

module.exports = {
  addCompanyImage,
  removeCompanyImage,
};

async function addCompanyImage(req, res, next) {
  try {
    if (!req?.files?.file?.[0]) {
      res.status(400);
      throw new Error('No file for upload passed');
    }
    if (!req?.params?.id) {
      res.status(400);
      throw new Error('No company ID passed for file upload');
    }
    if (!await _checkCompanyId(req.params.id)) {
      res.status(404);
      throw new Error(`No company with ID ${req.params.id}`);
    }

    const file = req.files.file[0];
    const fileExtention = path.extname(file.originalname).toLowerCase();
    const tempFilePath = file.path;

    if (!(fileExtention === '.png' || fileExtention === '.jpg' || fileExtention === '.jpeg' || fileExtention === '.gif')) {
      _remove(tempFilePath).catch((err) => { logger.error(err); });
      res.status(400);
      throw new Error('Only image files are allowed');
    }
  } catch (err) {
    const error = err.toString();
    logger.error(error);
    return res.json({ error });
  }

  return next();
}

async function removeCompanyImage(req, res, next) {
  try {
    if (!req?.params?.id) {
      res.status(400);
      throw new Error('No company ID passed');
    }
    if (!req?.params?.image_name) {
      res.status(400);
      throw new Error('No image name passed');
    }
    if (!await _checkCompanyId(req.params.id)) {
      res.status(404);
      throw new Error(`No company with ID ${req.params.id}`);
    }
    if (!await _checkImageName(req.params.image_name)) {
      res.status(404);
      throw new Error(`No photo with name ${req.params.image_name}`);
    }
  } catch (err) {
    const error = err.message;
    logger.error(error);
    return res.json({ error });
  }

  return next();
}

async function _checkCompanyId(companyId) {
  try {
    const company = await Company.findOne({ _id: companyId });
    return !!company;
  } catch (error) {
    return false;
  }
}

async function _checkImageName(imageName) {
  try {
    const photo = await Photo.findOne({ name: imageName });
    return !!photo;
  } catch (error) {
    return false;
  }
}

async function _remove(file) {
  return new Promise((resolve, reject) => {
    fs.unlink(file, (err) => {
      if (err) reject(err);
      else resolve();
    });
  });
}
