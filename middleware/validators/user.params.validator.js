module.exports = (req, res, next) => {
  try {
    if (!req?.body?.login) {
      res.status(400);
      throw new Error('login is empty');
    }

    if (!req?.body?.password) {
      res.status(400);
      throw new Error('password is empty');
    }
  } catch (err) {
    const error = err.message;
    return res.json({ error });
  }

  return next();
};
