const mongoose = require('mongoose');
const connection = require('../libs/connection');

const companySchema = new mongoose.Schema({
  contactId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Contact',
  },
  name: String,
  shortName: String,
  businessEntity: String,
  contract: {
    no: {
      type: String,
      default: 'б/н',
    },
    issue_date: {
      type: Date,
      default: new Date(),
    },
  },
  type: {
    type: Array,
    default: [],
  },
  status: String,
  address: String,
}, {
  timestamps: true,
  toJSON: { virtuals: true },
  toObject: { virtuals: true },
});

companySchema.virtual('photos', {
  ref: 'Photo',
  localField: '_id',
  foreignField: 'companyId',
});

module.exports = connection.model('Company', companySchema);
