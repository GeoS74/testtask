const mongoose = require('mongoose');
const connection = require('../libs/connection');

const contactSchema = new mongoose.Schema({
  lastname: String,
  firstname: String,
  patronymic: String,
  phone: String,
  email: {
    type: String,
    validate: [
      {
        validator(value) {
          return /^[-.\w]+@([\w-]+\.)+[\w-]{2,12}$/.test(value);
        },
        message: 'Некорректный {PATH}',
      },
    ],
  },
}, {
  timestamps: true,
});

module.exports = connection.model('Contact', contactSchema);
