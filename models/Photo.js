const mongoose = require('mongoose');
const connection = require('../libs/connection');

const photoSchema = new mongoose.Schema({
  name: String,
  filepath: String,
  thumbpath: String,
  companyId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Company',
  },
}, {
  timestamps: true,
});

module.exports = connection.model('Photo', photoSchema);
