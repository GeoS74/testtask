const { Router } = require('express');
const bodyParser = require('body-parser');

const userParamsValidator = require('../middleware/validators/user.params.validator');
const authController = require('../controllers/auth.controller');

const router = Router();

router.use(bodyParser.urlencoded({ extended: false }));

router.post(
  '/',
  userParamsValidator,
  authController.authorized,
);

router.post(
  '/signup',
  userParamsValidator,
  authController.signup,
);

module.exports = router;
