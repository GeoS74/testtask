const express = require('express');
const multer = require('multer');
const bodyParser = require('body-parser');

const config = require('../config.json');

const fileHandler = multer({ dest: config.uploads_dir });
const router = express.Router();

const auth = require('../middleware/auth.middleware');
const filesParamsValidator = require('../middleware/validators/files.params.validator');
const companiesParamsValidator = require('../middleware/validators/companies.params.validator');
const companiesController = require('../controllers/companies.controller');
const filesController = require('../controllers/files.controller');

router.use(bodyParser.urlencoded({ extended: false }));

router.post(
  '/',
  auth,
  companiesParamsValidator.addCompany,
  companiesController.add,
);

router.post(
  '/:id/image',
  auth,
  fileHandler.fields([{ name: 'file', maxCount: 1 }]),
  filesParamsValidator.addCompanyImage,
  filesController.saveImage,
);

router.patch(
  '/:id',
  auth,
  companiesParamsValidator.updateCompany,
  companiesController.update,
);

router.delete(
  '/:id/image/:image_name',
  auth,
  filesParamsValidator.removeCompanyImage,
  filesController.removeImage,
);

router.delete(
  '/:id',
  auth,
  companiesParamsValidator.deleteCompany,
  companiesController.del,
);

router.get(
  '/',
  auth,
  companiesController.getAll,
);

router.get(
  '/:id',
  auth,
  companiesController.get,
);

module.exports = router;
