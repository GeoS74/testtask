const express = require('express');
const bodyParser = require('body-parser');

const auth = require('../middleware/auth.middleware');
const contactsParamsValidator = require('../middleware/validators/contacts.params.validator');
const contactsController = require('../controllers/contacts.controller');

const router = express.Router();

router.use(bodyParser.urlencoded({ extended: false }));

router.post(
  '/',
  auth,
  contactsController.add,
);

router.patch(
  '/:id',
  auth,
  contactsParamsValidator,
  contactsController.update,
);

router.delete(
  '/:id',
  auth,
  contactsParamsValidator,
  contactsController.del,
);

router.get(
  '/',
  auth,
  contactsController.getAll,
);

router.get(
  '/:id',
  auth,
  contactsParamsValidator,
  contactsController.get,
);

module.exports = router;
