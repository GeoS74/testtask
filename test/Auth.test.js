const { expect } = require('chai');
const fetch = require('node-fetch');
const jwt = require('jsonwebtoken');
const User = require('../models/User');
require('../app');

const config = require('../config.json');

describe('testtask/test/Auth.test.js', () => {
  const testUser = {
    login: 'test_user',
    password: '123',
  };

  after(async () => {
    await User.deleteMany({ name: testUser.login });
  });

  describe('регистрация/авторизация пользователя', () => {
    it('валидация логина', async () => {
      const response = await fetch(`http://localhost:${config.port}/auth`, {
        headers: { 'Content-Type': 'application/json' },
        method: 'POST',
        body: JSON.stringify({}),
      });

      expect(response.status, 'если логин не передаётся ответ 400').to.be.equal(400);
      expect(
        await response.json(),
        'ответ сервера содержит объект',
      ).to.have.property('error');
    });

    it('валидация пароля', async () => {
      const response = await fetch(`http://localhost:${config.port}/auth`, {
        headers: { 'Content-Type': 'application/json' },
        method: 'POST',
        body: JSON.stringify({ login: 'foo' }),
      });

      expect(response.status, 'если пароль не передаётся ответ 400').to.be.equal(400);
      expect(
        await response.json(),
        'ответ сервера содержит объект',
      ).to.have.property('error');
    });

    it('регистрация пользователя', async () => {
      const response = await fetch(`http://localhost:${config.port}/auth/signup`, {
        headers: { 'Content-Type': 'application/json' },
        method: 'POST',
        body: JSON.stringify(testUser),
      });

      expect(response.status, 'сервер возвращает статус 201').to.be.equal(201);
      expect(
        await response.json(),
        'сервер возвращает объект',
      ).to.have.property('message');
    });

    it('повторная регистрация существующего пользователя', async () => {
      const response = await fetch(`http://localhost:${config.port}/auth/signup`, {
        headers: { 'Content-Type': 'application/json' },
        method: 'POST',
        body: JSON.stringify(testUser),
      });

      expect(response.status, 'сервер возвращает статус 400').to.be.equal(400);
      expect(
        await response.json(),
        'сервер возвращает объект',
      ).to.have.property('error');
    });

    it('авторизация пользователя', async () => {
      const response = await fetch(`http://localhost:${config.port}/auth/`, {
        headers: { 'Content-Type': 'application/json' },
        method: 'POST',
        body: JSON.stringify(testUser),
      });

      expect(response.status, 'сервер возвращает статус 200').to.be.equal(200);
      expect(
        await response.json(),
        'сервер возвращает объект',
      ).to.have.property('message');

      expect(
        response.headers.get('authorization'),
        'сервер возвращает заголовок Authorization',
      ).to.be.not.empty;

      const token = response.headers.get('authorization').split(' ')[1];
      if (!token) {
        throw new Error('сервер возвращает jwt-token');
      }

      try {
        jwt.verify(token, config.app);
      } catch (error) {
        throw new Error('сервер возвращает валидный jwt-token');
      }
    });

    it('проверка правильности логина пользователя', async () => {
      const response = await fetch(`http://localhost:${config.port}/auth`, {
        headers: { 'Content-Type': 'application/json' },
        method: 'POST',
        body: JSON.stringify({ login: '_test_user', password: '123' }),
      });

      expect(response.status, 'если пользователь не найден ответ 400').to.be.equal(400);
      expect(
        await response.json(),
        'ответ сервера содержит объект',
      ).to.have.property('error');
    });

    it('проверка правильности пароля пользователя', async () => {
      const response = await fetch(`http://localhost:${config.port}/auth`, {
        headers: { 'Content-Type': 'application/json' },
        method: 'POST',
        body: JSON.stringify({ login: 'test_user', password: '321' }),
      });

      expect(response.status, 'если пароль не верный ответ 400').to.be.equal(400);
      expect(
        await response.json(),
        'ответ сервера содержит объект',
      ).to.have.property('error');
    });
  });
});
