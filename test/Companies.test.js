const { expect } = require('chai');
const fetch = require('node-fetch');
const User = require('../models/User');
const Contact = require('../models/Contact');
const Company = require('../models/Company');
require('../app');

const config = require('../config.json');

describe('testtask/test/Companies.test.js', () => {
  let _authHeader;

  const testUser = {
    login: 'test_user',
    password: '123',
  };

  const testContact = {
    lastname: 'test_lastname',
  };

  const testCompany = {
    contactId: null,
    name: 'test_companyName',
    shortName: 'test_companyShortName',
    businessEntity: 'test_companybusinessEntity',
    contract: {
      no: 'test_companyContractNo',
      issue_date: new Date(),
    },
    type: ['test_companyType'],
    status: 'test_companyStatus',
    address: 'test_companyAddress',
  };

  before(async () => {
    await fetch(`http://localhost:${config.port}/auth/signup`, {
      headers: { 'Content-Type': 'application/json' },
      method: 'POST',
      body: JSON.stringify(testUser),
    });

    const responseAuth = await fetch(`http://localhost:${config.port}/auth/`, {
      headers: { 'Content-Type': 'application/json' },
      method: 'POST',
      body: JSON.stringify(testUser),
    });
    _authHeader = responseAuth.headers.get('authorization');

    await fetch(`http://localhost:${config.port}/contacts`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: _authHeader,
      },
      method: 'POST',
      body: JSON.stringify(testContact),
    });

    const contact = await Contact.findOne({ lastname: testContact.lastname });
    testCompany.contactId = contact.id;

    for (let i = 0; i++ < 3;) {
      testCompany.status += i;
      testCompany.type[0] += i;
      await fetch(`http://localhost:${config.port}/companies`, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: _authHeader,
        },
        method: 'POST',
        body: JSON.stringify(testCompany),
      });
    }
  });

  after(async () => {
    await User.deleteMany({ name: testUser.login });
    await Contact.deleteMany({ lastname: testContact.lastname });
    await Company.deleteMany({ name: testCompany.name });
  });

  describe('добавление/изменение компании', () => {
    it('добавление компании', async () => {
      const response = await fetch(`http://localhost:${config.port}/companies`, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: _authHeader,
        },
        method: 'POST',
        body: JSON.stringify(testCompany),
      });

      expect(response.status, 'сервер возвращает статус 201').to.be.equal(201);
      expect(
        await response.json(),
        'сервер возвращает объект',
      ).to.have.property('message');
    });

    it('изменение компании', async () => {
      const company = await Company.findOne({ name: testCompany.name });

      const response = await fetch(`http://localhost:${config.port}/companies/${company.id}`, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: _authHeader,
        },
        method: 'PATCH',
        body: JSON.stringify(testContact),
      });

      expect(response.status, 'сервер возвращает статус 200').to.be.equal(200);
      expect(
        await response.json(),
        'сервер возвращает объект с обновленными данными',
      )
        .that.is.an('object')
        .to.have.keys(['id', 'photos', 'createdAt', 'updatedAt', ...Object.keys(testCompany)]);
    });

    it('получение списка компаний', async () => {
      const response = await _getCompany(`http://localhost:${config.port}/companies/`);
      const company = await response.json();

      expect(response.status, 'сервер возвращает статус 200').to.be.equal(200);
      expect(
        company,
        'сервер возвращает массив с данными',
      ).that.is.an('array');
      expect(
        company[0],
        'ответ содержит ключи',
      )
        .that.is.an('object')
        .to.have.keys(['id', 'photos', 'createdAt', 'updatedAt', ...Object.keys(testCompany)]);
    });

    it('лимитирование выборки', async () => {
      const response = await _getCompany(`http://localhost:${config.port}/companies?limit=2`);
      const company = await response.json();

      expect(
        company,
        'сервер возвращает массив из 2-х элементов',
      ).to.have.length(2).that.is.an('array');
    });

    it('выбор компаний по статусу', async () => {
      const response = await _getCompany(`http://localhost:${config.port}/companies?status=${testCompany.status}`);
      const company = await response.json();

      for (let i = 0; i < company.length; i++) {
        expect(
          company[i].status,
          'сервер возвращает компании по статусу',
        ).to.be.equal(testCompany.status);
      }
    });

    it('выбор компаний по типу', async () => {
      const response = await _getCompany(`http://localhost:${config.port}/companies?type=${testCompany.type[0]}`);
      const company = await response.json();

      for (let i = 0; i < company.length; i++) {
        expect(
          company[i].type.indexOf(testCompany.type[0]),
          'сервер возвращает компании по типу',
        ).to.not.equal(-1);
      }
    });

    it('пагинация выборки', async () => {
      const response = await _getCompany(`http://localhost:${config.port}/companies`);
      const company = await response.json();

      const responseSkip = await _getCompany(`http://localhost:${config.port}/companies?start=1`);
      const companySkip = await responseSkip.json();

      expect(
        company[0].id,
        'сервер возвращает компании с указаным смещением',
      ).to.not.equal(companySkip[0].id);
    });

    it('сортировка по имени', async () => {
      const response = await _getCompany(`http://localhost:${config.port}/companies`);
      const company = await response.json();
      company.sort(_fieldSort('name'));

      const responseSortByName = await _getCompany(`http://localhost:${config.port}/companies?sort=name`);
      const companySortByName = await responseSortByName.json();

      for (let i = 0; i < company.length; i++) {
        expect(
          company[i].name,
          'сервер возвращает список компаний отсортированный по имени',
        ).to.equal(companySortByName[i].name);
      }
    });

    it('сортировка по дате', async () => {
      const response = await _getCompany(`http://localhost:${config.port}/companies`);
      const company = await response.json();
      company.sort(_fieldSort('createdAt'));

      const responseSortByDate = await _getCompany(`http://localhost:${config.port}/companies?sort=date`);
      const companySortByDate = await responseSortByDate.json();

      for (let i = 0; i < company.length; i++) {
        expect(
          company[i].createdAt,
          'сервер возвращает список компаний отсортированный по дате',
        ).to.equal(companySortByDate[i].createdAt);
      }
    });

    it('изменение порядка сортировки', async () => {
      const response = await _getCompany(`http://localhost:${config.port}/companies`);
      const company = await response.json();
      company.sort(_fieldSortReverse('id'));

      const responseOrder = await _getCompany(`http://localhost:${config.port}/companies?order=1`);
      const companyOrder = await responseOrder.json();

      for (let i = 0; i < company.length; i++) {
        expect(
          company[i].createdAt,
          'сервер возвращает список в обратной сортировке',
        ).to.equal(companyOrder[i].createdAt);
      }
    });

    it('получение одной компании', async () => {
      const company = await Company.findOne({ name: testCompany.name });

      const response = await _getCompany(`http://localhost:${config.port}/companies/${company.id}`);

      expect(response.status, 'сервер возвращает статус 200').to.be.equal(200);
      expect(
        await response.json(),
        'сервер возвращает массив с данными',
      )
        .that.is.an('object')
        .to.have.keys(['id', 'photos', 'createdAt', 'updatedAt', ...Object.keys(testCompany)]);
    });

    it('запрос компании по не валидному идентификатору', async () => {
      const company = await Company.findOne({ name: testCompany.name });
      const response = await _getCompany(`http://localhost:${config.port}/companies/${company.id}123`);

      expect(response.status, 'сервер возвращает статус 404').to.be.equal(404);
    });

    it('получение не существующей компании', async () => {
      const company = await Company.findOne({ name: testCompany.name });
      const response = await _getCompany(`http://localhost:${config.port}/companies/aaaaaaaaaaaaaaaaaaaaaaaa`);

      expect(response.status, 'сервер возвращает статус 404').to.be.equal(404);
    });

    it('удаление компании', async () => {
      const company = await Company.findOne({ name: testCompany.name });

      const response = await fetch(`http://localhost:${config.port}/companies/${company.id}`, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: _authHeader,
        },
        method: 'DELETE',
      });

      expect(response.status, 'сервер возвращает статус 200').to.be.equal(200);
      expect(
        await response.json(),
        'сервер возвращает объект',
      ).to.have.property('message');
    });
  });

  function _fieldSort(field) {
    return (a, b) => {
      if (a[field] < b[field]) return 1;
      if (a[field] > b[field]) return -1;
      return 0;
    };
  }
  function _fieldSortReverse(field) {
    return (a, b) => {
      if (a[field] > b[field]) return 1;
      if (a[field] < b[field]) return -1;
      return 0;
    };
  }

  function _getCompany(url) {
    return fetch(url, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: _authHeader,
      },
      method: 'GET',
    });
  }
});
