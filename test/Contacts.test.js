const { expect } = require('chai');
const fetch = require('node-fetch');
const User = require('../models/User');
const Contact = require('../models/Contact');
require('../app');

const config = require('../config.json');

describe('testtask/test/Contact.test.js', () => {
  let _authHeader;

  const testUser = {
    login: 'test_user',
    password: '123',
  };

  const testContact = {
    lastname: 'test_lastname',
    firstname: 'test_lastname',
    patronymic: 'test_lastname',
    phone: 'test_lastname',
    email: 'test_lastname@test_lastname.ru',
  };

  before(async () => {
    await fetch(`http://localhost:${config.port}/auth/signup`, {
      headers: { 'Content-Type': 'application/json' },
      method: 'POST',
      body: JSON.stringify(testUser),
    });

    const response = await fetch(`http://localhost:${config.port}/auth/`, {
      headers: { 'Content-Type': 'application/json' },
      method: 'POST',
      body: JSON.stringify(testUser),
    });
    _authHeader = response.headers.get('authorization');

    await fetch(`http://localhost:${config.port}/contacts`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: _authHeader,
      },
      method: 'POST',
      body: JSON.stringify(testContact),
    });
  });

  after(async () => {
    await User.deleteMany({ name: testUser.login });
    await Contact.deleteMany({ lastname: testContact.lastname });
  });

  describe('добавление/изменение контактов', () => {
    it('добавление контакта', async () => {
      const response = await fetch(`http://localhost:${config.port}/contacts`, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: _authHeader,
        },
        method: 'POST',
        body: JSON.stringify(testContact),
      });

      expect(response.status, 'сервер возвращает статус 201').to.be.equal(201);
      expect(
        await response.json(),
        'сервер возвращает объект',
      ).to.have.property('message');
    });

    it('изменение контакта', async () => {
      const contact = await Contact.findOne({ lastname: testContact.lastname });

      const response = await fetch(`http://localhost:${config.port}/contacts/${contact.id}`, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: _authHeader,
        },
        method: 'PATCH',
        body: JSON.stringify(testContact),
      });

      expect(response.status, 'сервер возвращает статус 200').to.be.equal(200);
      expect(
        await response.json(),
        'сервер возвращает объект с обновленными данными',
      ).to.have.property('id');
    });

    it('получение списка контактов', async () => {
      const response = await fetch(`http://localhost:${config.port}/contacts/`, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: _authHeader,
        },
        method: 'GET',
      });

      const contacts = await response.json();
      expect(response.status, 'сервер возвращает статус 200').to.be.equal(200);
      expect(
        contacts,
        'сервер возвращает массив с данными',
      ).that.is.an('array');
      expect(
        contacts[0],
        'ответ содержит ключи',
      )
        .that.is.an('object')
        .to.have.keys(['id', 'createdAt', 'updatedAt', ...Object.keys(testContact)]);
    });

    it('получение одного контакта', async () => {
      const contact = await Contact.findOne({ lastname: testContact.lastname });

      const response = await fetch(`http://localhost:${config.port}/contacts/${contact.id}`, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: _authHeader,
        },
        method: 'GET',
      });

      expect(response.status, 'сервер возвращает статус 200').to.be.equal(200);
      expect(
        await response.json(),
        'сервер возвращает массив с данными',
      )
        .that.is.an('object')
        .to.have.keys(['id', 'createdAt', 'updatedAt', ...Object.keys(testContact)]);
    });

    it('удаление контакта', async () => {
      const contact = await Contact.findOne({ lastname: testContact.lastname });

      const response = await fetch(`http://localhost:${config.port}/contacts/${contact.id}`, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: _authHeader,
        },
        method: 'DELETE',
      });

      expect(response.status, 'сервер возвращает статус 200').to.be.equal(200);
      expect(
        await response.json(),
        'сервер возвращает объект',
      ).to.have.property('message');
    });
  });
});
